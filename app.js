var express = require('express');
var tweetsController= require('./controllers/tweetsController');


var app = express();

//set-up template engine
app.set('view engine','pug');

//middleware to serve static file (eg stylesheets)
app.use(express.static('./public'));

//fire controllers
tweetsController(app);


//listen to port 3000
app.listen(3000);
console.log('App running on port 3000');